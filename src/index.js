console.log('index'); /**kr */
const qText = document.querySelector('.question');
const qImg = document.querySelector('.img_qcm');
const qChoice = document.querySelectorAll('.choice');
const start = document.querySelector('.startHide');
const hide = document.querySelectorAll('.hide');
const showScore = document.querySelector('.score');
const finalScore = document.querySelector('.finalScore');
const box = document.querySelector('.bg_qcm');
const threeC = document.querySelector('.threeC');
const textA = document.querySelector('.textA');
const input = document.querySelector('.input');
const timerText = document.querySelector('.barText');
const s2 = document.querySelector('.s2');
const replay = document.querySelector('.replay');
const fourC = document.querySelector('.fourC');
const qMulti = document.querySelectorAll('.multi');
const multiDis = document.querySelectorAll('.multiDis');
let timer = 100;



/**set the default parameters for the timer and the display of hiddens elements */
threeC.style.display = 'none';
textA.style.display = 'none';
fourC.style.display = 'none';

/**show the game after clicking the start button */
start.addEventListener('click', () => {
    timerCountdown();
    nextQ();
    start.style.display = 'none';
});


let questions = [{
        ask: 'Dans "Berserk" de Kentaro Miura, comment s\'appelle le groupe auquel appartiennent Guts et Griffith durant leur jeunesse ?',
        imgQ: 'img/berserk.jpg',
        choice: ["L'armée du midland", "La bande du faucon", "Les Taureaux Noirs"],
        correct: "2",
        system: "buttons"
    },
    {
        ask: 'Avec quel mangaka est mariée Naoko Takeuchi, créatrice de "Sailor Moon" ?',
        imgQ: 'img/sailor.jpg',
        choice: ["Yoshihiro Togashi", "Takehiko Inoue", "Kishibe Rohan"],
        correct: "1",
        system: "buttons"
    },
    {
        ask: 'Quel est le nom du célèbre stand de DIO dans la partie 3 de  JoJo\'s Bizarre Adventure ?',
        imgQ: 'img/dio.jpg',
        correct: "the world",
        system: "text"
    },
    {
        ask: 'Quel est le manga le plus vendu au monde ?',
        imgQ: 'img/money.jpg',
        choice: ["Dragon Ball", "Deuxième mi-temps de Pierre Ménès", "One Piece"],
        correct: "3",
        system: "buttons"
    },
    {
        ask: 'Dans "Monster" de Naoki Urasawa, Johann pour se venger de Tenma créa un plan, quel est son nom ?',
        imgQ: 'img/monster.jpg',
        choice: ["Le meurtre parfait", "Le suicide parfait", "Le parfait à la vanille"],
        correct: "2",
        system: "buttons"
    },
    {
        ask: 'Dans "D.Gray-Man" de Katsura Hoshino, quel est le nom du compagnon volant d\'Allen ?',
        imgQ: 'img/dgm.jpg',
        choice: ["Timcanpy", "Ur-Canpy", "Timcamping (indice : non)"],
        correct: "1",
        system: "buttons"
    },
    {
        ask: 'Quel est le nom humain du robot "Origin" du manga éponyme ?',
        imgQ: 'img/origin.jpg',
        choice: ["Tanaka Jin", "Tanuki Jing", "Thierry"],
        correct: "1",
        system: "buttons"
    },
    {
        ask: 'Comment est nommée l\'organisation criminelle la plus dangereuse de la tour dans Tower of God ?',
        imgQ: 'img/tog.png',
        correct: "fug",
        system: "text"
    },
    {
        ask: 'Dans "Vinland Saga" de Yukimura Makoto quel est le nom du père de Thorfinn ?',
        imgQ: 'img/vinland.jpeg',
        correct: "thors",
        system: "text"
    },
    {
        ask: 'De quel oeuvre le manga "Pluto" de Naoki Urasawa est-il une adaptation ?',
        imgQ: 'img/pluto.jpg',
        choice: ["Astro Boy", "Astroworld", "AstraZeneca"],
        correct: "1",
        system: "buttons"
    },
    {
        ask: 'Comment se nomme le héros du manga éponyme "Naruto" (gratuit ce point) ?',
        imgQ: 'img/naruto.jpg',
        correct: "naruto",
        system: "text"
    },
    {
        ask: 'Parmi ces 4 noms, lesquels appartiennent aux membres de la famille Zoldyck d\'"Hunter x Hunter" ? (Trois réponses)',
        imgQ: 'img/zoldyck.jpg',
        choice: ["Killua", "Illumi", "Killlakill", "Milluki", ],
        correct: [1, 2, 4],
        system: "multiple"
    },
    {
        ask: 'Comment s\'appelle l\'enfant dans "L\'Enfant et le Maudit" de Nagabe ?',
        imgQ: 'img/nagabe.jpg',
        choice: ["Chiva", "Sheeva", "ShivFPS"],
        correct: "2",
        system: "buttons"
    },
    {
        ask: 'Quel concept géométrique est utilisé par Gyro Zepelli pour améliorer la rotation de ses steel balls dans la partie 7 de JoJo',
        imgQ: 'img/jojo.jpg',
        choice: ["Pi", "Le théorème de Thalès", "Le rectangle d'or"],
        correct: "3",
        system: "buttons"
    },
    {
        ask: 'Combien de trèfles comporte le grimoire d\'Asta dans "Black Clover" de Yūki Tabata ?',
        imgQ: 'img/black.jpg',
        choice: ["6", "5", "4"],
        correct: "2",
        system: "buttons"
    },
    {
        ask: 'Dans "Tokyo revengers" de combien d\'années Takemichi peut-il revenir dans le temps ? (exprimer en chiffres)',
        imgQ: 'img/revengers.jpg',
        correct: "12",
        system: "text"
    },
    {
        ask: 'De quand date le premier manga ?',
        imgQ: 'img/manga.jpg',
        choice: ["1789", "2004", "1902"],
        correct: "3",
        system: "buttons"
    },
    {
        ask: 'Lesquels de ces personnages font parties de l\'akatsuki dans "Naruto" ? (Deux réponses)',
        imgQ: 'img/akatsuki.png',
        choice: ["Kisame", "Saitama", "Deidara", "Philippe Poutou", ],
        correct: [1, 3],
        system: "multiple"
    },

];

/**
 * shuffle the questions array so that it's never the same order
 * @param {Array} array 
 */
function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}


shuffle(questions);

let arrA = []; // array needed for the checkbox answers
let x = -1; //index for the questions loop, used to check the answer too that's why he's here

/** score & checkAnswers */

/**buttons check */

for (let n = 0; n < qChoice.length; n++) {
    const element2 = qChoice[n];
    element2.addEventListener('click', () => {
        checkAnswer(n + 1);
    });

}

/** text check */
textA.addEventListener('submit', (event) => {
    event.preventDefault();
    checkAnswer(input.value.toLowerCase());
});


/** checkbox check */

for (let l = 0; l < qMulti.length; l++) {
    const element = qMulti[l];
    element.addEventListener('change', function () {
        if (this.checked) {
            arrayAdd(l + 1);
        } else {
            arrayDel(l + 1);
        }
    });

}

let score = 0;

/**
 * check if the button pressed correspond to the 'correct' propriety in the question object and go to the next question
 * @param {any} i 
 */
function checkAnswer(i) {
    if (Array.isArray(i) === true) {
        let iReduced = i.reduce((a, b) => a + b, 0);
        let qReduced = questions[x].correct.reduce((a, b) => a + b, 0);
        if (iReduced === qReduced) {
            score++;
            showScore.textContent = score + ' points';
        } else {
            console.log('raté');
            box.classList.remove('shake');
            setTimeout(function () {
                box.classList.add('shake');
            }, 2);
        }
        qMulti.forEach(element => {
            if (element.checked) {
                element.checked = false;

            }
        });
    } else {
        if (i == questions[x].correct) {
            score++;
            showScore.textContent = score + ' points';
        } else {
            console.log('raté');
            box.classList.remove('shake');
            setTimeout(function () {
                box.classList.add('shake');
            }, 2);
        }
    }

    input.value = '';
    arrA = [];
    nextQ();

}



/**
 * for every click or submit the index change by one and show a new question 
 * the function determine what type of questions needs to be displayed via the "system" propoerty in the objects
 */
function nextQ() {
    if (x < questions.length - 1) {
        hide.forEach(item => {
            item.style.display = "flex";
        });
        x++;
        qText.textContent = questions[x].ask;
        qImg.src = questions[x].imgQ;
        if (questions[x].system === "buttons") {
            threeC.style.display = 'flex';
            textA.style.display = 'none';
            fourC.style.display = 'none';
            for (let index = 0; index < qChoice.length; index++) {
                const disChoice = qChoice[index];
                disChoice.textContent = questions[x].choice[index];
            }
        } else if (questions[x].system === "text") {
            threeC.style.display = 'none';
            textA.style.display = 'flex';
            fourC.style.display = 'none';
        } else if (questions[x].system === "multiple") {
            threeC.style.display = 'none';
            textA.style.display = 'none';
            fourC.style.display = 'flex';
            for (let index = 0; index < multiDis.length; index++) {
                const disM = multiDis[index];
                disM.textContent = questions[x].choice[index];
            }
        }
    } else {
        threeC.style.display = 'none';
        textA.style.display = 'none';
        console.log('plus rien');
        endGame();
    }

}

/**checkboxes functions */

/**
 * add the chackbox answers to an array for them to be verified by the system
 * @param {number} g 
 */
function arrayAdd(g) {
    arrA.push(g);
    if (arrA.length === questions[x].correct.length) {
        checkAnswer(arrA);
    }
}


function arrayDel(h) {
    arrA = arrA.filter(function (number) {
        return number != h;
    });
}

/**
 * Launch the timer by decreasing the 'timer' variable until it reaches 0
 */
function timerCountdown() {
    window.intervalCT = setInterval(() => {
        if (timer > 0) {
            timer = timer - 0.1111;
            timerText.style.width = timer + '%';

        } else {
            console.log('timeout');
            setTimeout(400, endGame()) //little timeout to prevent some possible bugs on the server version, ty Math for that
        }
    }, 100);
}

replay.addEventListener('click', () => {
    window.location.reload();
    return false;
});

/**
 * End the game (this as nothing to do with the last Avengers)
 */
function endGame() {
    finalText(score);
    s2.style.width = "100vw";
    clearInterval(intervalCT);
}

/**
 * show the final score
 * @param {number} score 
 */
function finalText(score) {
    if (score <= 5) {
        finalScore.textContent = 'Score final = ' + score + ' | ' + questions.length + ', tu peux mieux faire.';
    } else if (score <= 10) {
        finalScore.textContent = 'Score final = ' + score + ' | ' + questions.length + ', c\'est passable.';
    } else if (score <= 15) {
        finalScore.textContent = 'Score final = ' + score + ' | ' + questions.length + ', pas mal du tout !';
    } else if (score === questions.length) {
        finalScore.textContent = 'Score final = ' + score + ' | ' + questions.length + ', excellent !';
    }
}



