# Project

I had to make a multiple choice quizz based on any subject whatsoever so I decided to do it on the mangas and the world around them.

## Realisation

I started with a wireframe for the global design idea :

[![wireframe.png](public/img/wireframe.png)](https://whimsical.com/qcm-CvXdf3Lpyq3jZutGgnh1v5)

Then I decided to use bootstrap to make the structure of the page, i made all of the necessaries elements like the buttons, checkboxes, input etc..
I used JS to hide/show them at the right time like after start was clicked or when a question needed a specific way to respond.

I verified the HTML syntax with W3C validator, corrected my mistakes then verified again until it showed no errors whatsoever.

For the JS part i used an array of objects to create the questions and their property (system used to respond, correct answer and differents choices) that i shuffled with a function every time the page is loaded so that it's never the same order.

## Final uploaded version

[LINK](https://kevinrsi.gitlab.io/QCM/)
